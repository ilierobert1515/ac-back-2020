const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), email  VARCHAR(255) , cnp  VARCHAR(20) , telefon  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10), varsta  VARCHAR(3), gen VARCHAR(3))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
    gen: req.body.gen,
  };
  let error = [];
  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ
 
  if(!bilet.nume||!bilet.prenume||!bilet.telefon||!bilet.cnp||!bilet.email||!bilet.data_inceput||!bilet.data_sfarsit||!bilet.varsta){
    console.log("Toate campurile trebuie completate!");
    error.push("Toate campurile trebuie completate!"); 
 }
 else{

   if(bilet.nume.length < 2 || bilet.nume.length > 31){
     console.log("Nume invalid!");
     error.push("Nume invalid!");
   }
   else{
     if(!bilet.nume.match("^[A-Za-z]+$")){
       console.log("Numele trebuie sa contina doar litere!");
       error.push("Numele trebuie sa contina doar litere!");
     }
   }

   if(bilet.prenume.length < 2 || bilet.prenume.length > 31){
     console.log("Prenume invalid!");
     error.push("Prenume invalid!");
   }
   else{
     if(!bilet.prenume.match("^[A-Za-z]+$")){
       console.log("Prenumele trebuie sa contina doar litere!");
       error.push("Prenumele trebuie sa contina doar litere!");
     }
   }

   if(bilet.telefon.length != 10){
      console.log("Telefonul trebuie sa contina 10 cifre!");
      error.push("Telefonul trebuie sa contina 10 cifre!");
   }
   else{
      if(!bilet.telefon.match("^[0-9]+$")){
       console.log("Telefonul trebuie sa contina doar cifre!");
       error.push("Telefonul trebuie sa contina doar cifre!");
      }
   }

   /*if(!bilet.email.match("^[^\s@]+@[^\s@]+\.[^\s@]+$")){
     console.log("Email invalid!");
     error.push("Email invalid!");
   } */

  function emailIsValid (email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }

  if(!emailIsValid(bilet.email)){
    console.log("Email invalid!");
    error.push("Email invalid!");
  }


   
   if(bilet.cnp.length != 13){
    console.log("CNP invalid!");
    error.push("CNP invalid!");
   }
  
  
   year = 0;
   month = 0;
   day = 0;
   if (bilet.cnp[1] < 2){
      year = 2000;
      year = year + bilet.cnp[1]*10;
      year = year/10;
      year = year + bilet.cnp[2];
    
   }
      else{
        year = 1900;
        year = year + bilet.cnp[1]*10;
        year = year/10;
        year = year + bilet.cnp[2];
     }
    month = bilet.cnp[3] + bilet.cnp[4];
    day = bilet.cnp[5] + bilet.cnp[6];
    //console.log(year,month,day);


   if(!bilet.varsta.match("^[0-9]+$")){
    console.log("Varsta trebuie sa contina doar cifre!");
    error.push("Varsta trebuie sa contina doar cifre!");
   }
   else{
     if(bilet.varsta.lenght < 1 || bilet.varsta.lenght > 3){
      console.log("Varsta trebuie sa fie cuprinsa intre 1 si 999!");
      error.push("Varsta trebuie sa fie cuprinsa intre 1 si 999!");
     }
   }

   if(bilet.cnp[0] ==  1 || bilet.cnp[0] ==  3 || bilet.cnp[0] ==  5){
      bilet.gen = "M";
   }
   else{
      if(bilet.cnp[0] ==  2 || bilet.cnp[0] ===  4 || bilet.cnp[0] ==  6){
         bilet.gen = "F";        
      }
    }
    
    function parseDate(str) {
      var mdy = str.split('/');
      return new Date(mdy[2], mdy[0]-1, mdy[1]);
  }
  
  var date_diff_indays = function(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
  }



  if(date_diff_indays(bilet.data_inceput, bilet.data_sfarsit) <= 0){
      console.log("Schimbati datele intre ele!");
      error.push("Schimbati datele intre ele!");
    }
    
    
  //Nu stiu sa aflu data de "azi" 


  $("input").on("change", function() {
    this.setAttribute(
        "data-date",
        moment(this.value, "YYYY-MM-DD")
        .format( this.getAttribute("data-date-format") )
    )
}).trigger("change")
      

 }

  if (error.length === 0) {

    const sql = `INSERT INTO abonamente_metrou (nume,
      prenume,
      telefon,
      cnp,
      email,
      data_inceput,
      data_sfarsit,
      varsta,
      gen) VALUES (?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.telefon,
        bilet.cnp,
        bilet.email,
        bilet.data_inceput,
        bilet.data_sfarsit,
        bilet.varsta,
        bilet.gen,
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html
